<?php

namespace frontend\widgets\maxSalary;

use Yii;
use yii\base\Widget;
use frontend\models\Employee;

/**
 * @author Sergij Sergij
 */
class MaxSalary extends Widget{
    
    public $max = null;
    public function run(){
    
        $max = Yii::$app->params['maxSalaryRows'];
        if ($this->max){
          $max = $this->max;  
        }
        $list = Employee::getMaxSalary($max);
        
        return $this->render('salarymax',[
            'list' => $list,
        ]);
    }
}

