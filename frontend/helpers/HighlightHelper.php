<?php

namespace frontend\helpers;

/**
 *
 * @author Sergij Sergij
 */
class HighlightHelper 
{
    // return string with tag <b>$keyword</b>
    
    //public static function process($keyword, $item)
    //{     
    //    return str_replace($keyword,'<b>'.$keyword.'</b>',$item);
    //}
    
    public static function process($text, $content)
    {
        $text = preg_quote($text);
        $words = explode(' ', trim($text));
        
        //echo '<pre>';
        //print_r($words);
        //echo '<pre>';die;
        return preg_replace('/' . implode('|', $words) . '/i', '<b>$0</b>', $content);
    }
}
