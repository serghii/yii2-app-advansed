<?php
use yii\helpers\Html;
?>
<?php
/* @var $model frontend\models\Comment*/
if ($model->hasErrors()) {
    echo '<pre>';
    print_r($model->getErrors());
    echo '</pre>';
}
//print_r($list);
?>
<div class="body-content">
        <div class="row">
            <div class="col-lg-4">
                <h4>Name</h4>
            </div>
            <div class="col-lg-8">
                <h4>Comment</h4>                 
            </div>
        </div>
</div>
<?php foreach ($list as $item): ?>
    <hr>
 <div class="body-content">
        <div class="row">
            <div class="col-lg-4">                
            <?php echo Html::encode($item['name'])?> 
            </div>
            <div class="col-lg-8">                
                <?php echo Html::encode($item['comment']);?> 
            </div>
        </div>
</div>

<?php endforeach; ?>

<h3>Write Your comment here!</h2>
<h5>* - required </h5>

<form method="post">
    <p>Name *:</p>
    <input name="name" type="text" />
    <br><br>
    
    <p>Email :</p>
    <input name="email" type="email" />
    <br><br>
    
    <p>Comment *:</p>
    <input name="comment" type="text" />
    <br><br>
    
    <input type="submit" />    

</form>

