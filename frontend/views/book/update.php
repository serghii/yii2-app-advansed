<?php
/* @var $this yii\web\View */
/* @var $model frontend\models\Book */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<h1>Update book #ID <?php echo $model->id; ?></h1>
<?php $form = ActiveForm::begin(); ?>
    
    <?php echo $form->field($model, 'name'); ?>
    
    <?php echo $form->field($model, 'isbn'); ?>
    
    <?php echo $form->field($model, 'date_published'); ?>

    <?php echo $form->field($model, 'publisher_id'); ?>

    <?php echo Html::submitButton('Save', [
        'class' => 'btn btn-primary',
    ]); ?>

<?php ActiveForm::end();
