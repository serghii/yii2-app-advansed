<?php
/* @var $this yii\web\View */
/* @var $booksList[] frontend\models\Author */
use yii\helpers\Url;
?>
<h1>Books</h1>
<br>

<a href="<?php echo Url::to(['book/create']); ?>" class="btn btn-primary">Create new book</a>

<br>
<br>
<table class="table table-condensed">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>ISBN</th>
            <th>Date published</th>
            <th>Publisher id</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    <?php foreach ($booksList as $book):?>
        <tr>
            <td><?php echo $book->id;?></td>
            <td><?php echo $book->name;?></td>
            <td><?php echo $book->isbn;?></td>
            <td><?php echo $book->date_published;?></td>
            <td><?php echo $book->publisher_id;?></td>
            <td><a href="<?php echo Url::to(['book/update', 'id' => $book->id]); ?>">Edit</a></td>
            <td><a href="<?php echo Url::to(['book/delete', 'id' => $book->id]); ?>">Delete</a></td>
        </tr>
    <?php endforeach; ?>
</table>