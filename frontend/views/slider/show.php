<?php
/*var $this yii\web\View*/
use frontend\assets\SliderAsset;

SliderAsset::register($this);
$this->registerJsFile('@web/js/slider/scripts.js', ['depends'=>
    SliderAsset::className()]);
?>
<div id="wrapper">
<div class="slider-wrapper theme-default">
<div id="slider" class="nivoSlider"> 
    <img src="/files/photo/1.jpg" data-thumb="/files/photo/1.jpg" alt="" /> 
    <img src="/files/photo/2.jpg" data-thumb="/files/photo/2.jpg" alt="" />
    <img src="/files/photo/3.jpg" data-thumb="/files/photo/3.jpg" alt="" /> 
    <img src="/files/photo/4.jpg" data-thumb="/files/photo/4.jpg" alt=""/>
    <img src="/files/photo/5.jpg" data-thumb="/files/photo/5.jpg" alt="" /> 
    <img src="/files/photo/6.jpg" data-thumb="/files/photo/6.jpg" alt=""/>
    <img src="/files/photo/7.jpg" data-thumb="/files/photo/7.jpg" alt="" /> 
    <img src="/files/photo/8.jpg" data-thumb="/files/photo/8.jpg" alt=""/>
    <img src="/files/photo/9.jpg" data-thumb="/files/photo/9.jpg" alt="" /> 
    <img src="/files/photo/10.jpg" data-thumb="/files/photo/10.jpg" alt=""/>
</div>
</div>
</div>


