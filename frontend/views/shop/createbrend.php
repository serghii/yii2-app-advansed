<?php

/* @var $this yii\web\View */
/* @var $brend frontend\models\Brend */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<p><?php echo ' Add brend hire.' ;  ?></p>
<?php $form = ActiveForm::begin(); ?>
    
    <?php echo $form->field($brend, 'name'); ?>
    
    <?php echo $form->field($brend, 'country'); ?>
   
    <?php echo Html::submitButton('Save', [
        'class' => 'btn btn-primary',
    ]); ?>

<?php ActiveForm::end();
