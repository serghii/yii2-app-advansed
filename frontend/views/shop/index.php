<?php
/* @var $this yii\web\View */
/* @var $productkList[] frontend\models\Product */
?>
<h1>Products</h1>
<?php foreach ($productList as $product): ?>

    <div class="col-md-10">
        <h3><?php echo $product->name; ?></h3>
        <p><?php echo $product->getDatePublished(); ?></p>    
        <p><?php echo $product->getPrice(); ?></p>
        <p><?php echo $product->getBrendName(); ?></p>
        <?php foreach ($product->getCategories() as $category): ?>
            <p><?php echo $category->getCategoryName($category)?></p>
<?php endforeach; ?>
        <hr>
    </div>

<?php endforeach;