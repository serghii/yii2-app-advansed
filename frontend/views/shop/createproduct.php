<?php

/* @var $this yii\web\View */
/* @var $product frontend\models\Product */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<p><?php echo ' Add product hire.' ;  ?></p>
<?php $form = ActiveForm::begin(); ?>
    
    <?php echo $form->field($product, 'name'); ?>
    
    <?php echo $form->field($product, 'articul'); ?>
    
    <?php echo $form->field($product, 'date_published'); ?>
    
    <?php echo $form->field($product, 'brend_id'); ?>
   
    <?php echo $form->field($product, 'price'); ?>

    <?php echo $form->field($product, 'rating'); ?>
   
    <?php echo Html::submitButton('Save', [
        'class' => 'btn btn-primary',
    ]); ?>

<?php ActiveForm::end();

