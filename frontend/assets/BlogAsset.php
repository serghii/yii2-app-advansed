<?php

namespace frontend\assets;

use yii\web\AssetBundle;
/**
 * Description of BlogAsset
 *
 * @author Sergij Sergij
 */
class BlogAsset extends AssetBundle{
    
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/blog/clean-blog.css',
        'css/blog/clean-blog.min.css',
        'vendor/bootstrap/css/bootstrap.css'
    ];
    public $js = [
        'js/blog/clean-blog.js',
        'js/blog/clean-blog.min.js',
        'js/blog/contact_me.js',
        'js/blogjqBootstapValidation.js',
        'vendor/bootstrap/js/bootstrap.js'
        
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
