<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Sergij Sergij
 */
class SliderAsset extends AssetBundle
{
    public $css = [
        'css/slider/nivo-slider.css',
        'css/slider/default.css',
    ];
    
    public $js = [
        'js/nivo/jquery.nivo.slider.js',
    ];
    
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
