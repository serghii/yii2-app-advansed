<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Product;
use frontend\models\Category;
use frontend\models\Brend;

class ShopController extends Controller
{
    public function actionIndex()
    {
        $productList = Product::find()->orderBy('id')->limit(30)->all();
        
        return $this->render('index',
            ['productList'=> $productList,
        ]);
    }
    
    public function actionCreateproduct()
    {
        $product = new Product();
        
        if ($product->load(Yii::$app->request->post()) && $product->save()) {
            Yii::$app->session->setFlash('success', 'Product saved!');
            return $this->refresh();
        }
        
        return $this->render('createproduct', [
            'product' => $product,
        ]);
    }
    
    public function actionCreatecategory()
    {
        $category = new Category();
        
        if ($category->load(Yii::$app->request->post()) && $category->save()) {
            Yii::$app->session->setFlash('success', 'Category created!');
            return $this->refresh();
        }
        
        return $this->render('createcategory', [
            'category' => $category,
        ]);
    }
    
    public function actionCreatebrend()
    {   
        $brend = new Brend();
       
        if ($brend->load(Yii::$app->request->post()) && $brend->save()) {
            Yii::$app->session->setFlash('success', 'Brend created!');
            return $this->refresh();
        }
        
        return $this->render('createbrend', [
            'brend' => $brend,
        ]);
    }
   

}   
