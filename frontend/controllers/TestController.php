<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Test;
use frontend\models\User;
use Faker\Factory;

class TestController extends Controller
{
    public function actionIndex()
    {
        $max = Yii::$app->params['maxNewsInList'];
        
        $list =Test::getNewsList($max);
        
        return $this->render('index',[
            'list' => $list,
        ]);
    }
    
    public function actionView($id)
    {
       $item = Test::getItem($id);
       
       return $this->render('view', [
           'item' => $item
       ]);
    }
    
    public function actionMail() 
    {
        $result = Yii::$app->mailer->compose()
                ->setFrom('tex0987@gmail.com')
                ->setTo('tex0987@gmail.com')
                ->setSubject('Тема сообщения')
                ->setTextBody('Текст сообщения')
                ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
                ->send();
        var_dump($result);
        die;
    }
    
   // public function actionLook()
   // {
    //    $str = 'When you pay that much you have to know that everyone';
    //    echo \yii\helpers\BaseStringHelper::truncateWords($str, 8,$suffix = '', $asHtml = false);
    //    echo '<hr>';
    //    echo strpos ( $str , ' ' , 19 );
    //    echo \yii\helpers\BaseStringHelper::truncate( $str, strpos ( $str , ' ' , 19 ), '', null, false );
    //    die;
    //}
    
    public function actionLook()
    {
      $max = Yii::$app->params['maxNewsInList'];
        
        $list =Test::getNewsListFullWord($max);
        
        return $this->render('look',[
            'list' => $list,
        ]);  
    }
    
    public function actionUser()
    {
        $user = new User;
        
        echo 'test<br>';
    }
    
    public function actionGenerate(){
              
            /* @var @faker Faker\Generator *instance*/
            $faker = Factory::create();
            
            for($j = 0; $j < 100; $j++) {
               
                $news = [];
                for($i = 0; $i < 100; $i++){
                    $news[] =  [$faker->text(rand(30,45)),$faker->text(rand(1000,2000)),rand(0,1)];  
            }
            Yii::$app->db->createCommand()->batchInsert('news', ['title', 'content', 'status'], $news)->execute();
            unset($news);
        }
        die('stop');
    }
}

