<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Book;
use frontend\models\Publisher;

class BookshopController extends \yii\web\Controller
{
    public function actionIndex()
    {
        //$condition = ['publisher_id' => 2];
        //$bookList = Book::find()->where($condition)->orderBy('date_published')->limit(3)->all();
        $bookList = Book::find()->orderBy('date_published')->limit(30)->all();
        // $book = new Book;
        //$book->name = 'Test book';
        //$book->isbn = '8686958776';
        //$book->save();
        
        //echo '<pre>';
        //print_r($book->getErrors());
        //echo '</pre>';
        
        return $this->render('index',
            ['bookList'=> $bookList,
        ]);
    }
    
    public function actionCreate()
    {
        $book = new Book();
        $publishers = Publisher::getList();
        if ($book->load(Yii::$app->request->post()) && $book->save()) {
            Yii::$app->session->setFlash('success', 'Saved!');
            return $this->refresh();
        }
        
        return $this->render('create', [
            'book' => $book,
            'publishers' => $publishers,
        ]);
    }

}
