<?php

namespace frontend\controllers;

use yii\web\Controller;
use frontend\models\Window;
use Yii;
use frontend\models\Sender;

/**
 * @author Sergij Sergij
 */
class OrderController extends Controller
{
    public function actionWindow()
    {
      $model = new Window();
      $model->scenario = Window::SCENARIO_ORDER_WINDOW;
      $formData = Yii::$app->request->post();
      if (Yii::$app->request->isPost && Sender::run($formData)) {
            
            $model->attributes = $formData;
             if ($model->validate() ) {
                Yii::$app->session->setFlash('success', 'The letter with your order sent to the administrator email. We connect with You shortly.');
            }
           
        }
       
       return $this->render('window', [
            'model' => $model,
        ]);
      
    }
}
