<?php

/*
 * Created by Serg.Bystrika at 23.07.2017
 * as task for the Lesson 9
 */

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Count;

/**
 * Description of CountController
 *
 * @author Sergij Sergij
 */
class CountController extends Controller{
    
    public function actionQuantity()
    {
        $tablename = Yii::$app->params['countRecordsTable'];
        $quantity = Count::getQuantityRecords($tablename);
        //print_r($quantity);die;
        return $this->render('show',[
            'quantity' => $quantity,
            'tablename' => $tablename,
        ]);
    }
}
