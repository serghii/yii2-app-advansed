<?php

namespace frontend\controllers;

use Yii;

/**
 * Description of SenderController
 *
 * @author Sergij Sergij
 */
class SenderController extends \yii\web\Controller
{
 
    public function actionWrite()
    {
        //Yii::setAlias('@log', '/var/www/project/frontend/web/log.txt');
        Yii::$app->formatter->timeZone = 'Europe/Kiev';
        $time = Yii::$app->formatter->asDate('now', 'php:d-m-Y H:i:s');
        //$file = '/var/www/project/frontend/web/log.txt';
        $file = Yii::getAlias('@log'); //@log in aliases main.php
        echo $time;
        $fp = fopen($file, 'a+');
        fwrite($fp, $time."\r\n");
        fclose($fp);
        die;
        
    }
}
