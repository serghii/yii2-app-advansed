<?php

namespace frontend\controllers;

use yii\web\Controller;
/**
 * @author Sergij Sergij
 */
class SliderController extends Controller
{
    public function actionShow()
    {
       return $this->render('show'); 
    }           
}