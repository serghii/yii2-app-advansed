<?php

namespace frontend\controllers;

use \yii\web\Controller;
/**
 * @author Sergij Sergij
 */
class BlogController extends Controller
{
    public function actionContact(){
        
        return $this->render('contact_me');
    }
    
    public function actionAbout(){
        
        return $this->render('about');
    }
    
    public function actionPost(){
        
        return $this->render('post');
    }
    
    public function actionIndex(){
        
        return $this->render('index');
    }
    
    
}
