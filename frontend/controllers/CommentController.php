<?php


namespace frontend\controllers;

use yii\web\Controller;
use frontend\models\Comment;
use Yii;

/**
 * @author Sergij Sergij
 */
class CommentController extends Controller
{
   public function actionRegister()
   {
       $model = new Comment();
       $model->scenario = Comment::SCENARIO_COMMENT_REGISTER;
       
       $formData = Yii::$app->request->post();
       
        if (Yii::$app->request->isPost) {
            
            $model->attributes = $formData;
             if ($model->validate() && $model->save()) {
                Yii::$app->session->setFlash('success', 'Comment publicated!');
            }
           
        }
        $list = Comment::getCommentList();
       
       return $this->render('register', [
            'model' => $model,
            'list' => $list,
        ]);
   }
}
