<?php
return [
    'adminEmail' => 'tex0987@gmail.com',
    'maxNewsInList' => 6,
    'shortTextLimit' => 20,
    'countRecordsTable' => 'news',
    'wordTextLimit' => 5,
    //'numberSymbols' => 29,
    'maxSalaryRows' => 1,
];
