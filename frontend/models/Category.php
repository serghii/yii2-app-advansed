<?php

namespace frontend\models;

use \yii\db\ActiveRecord;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $identity_number
 */
class Category extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{category}}';
    }
    
    public function rules()
    {
        return [
            [['name', 'identity_number'], 'required'],
            [['identity_number'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'identity_number' => 'Identity Number',
        ];
    }
    
    /**
     * @return string
     */
    public function getCategoryName($category)
    {
        if ($category->name) {
            return $category->name;
        }
        return "Not set";
    }
}
