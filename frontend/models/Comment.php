<?php

namespace frontend\models;

use yii\base\Model;
use frontend\models\Comment;
use Yii;
/**
 * @author Sergij Sergij
 */
class Comment extends Model
{
 const SCENARIO_COMMENT_REGISTER = 'comment_register';
    const SCENARIO_COMMENT_UPDATE = 'comment_update';
    public $name;
    public $email;
    public $comment;
    
    public function scenarios()
    {
        
        return [
            self::SCENARIO_COMMENT_REGISTER => ['name', 'email',  'comment'],
            self::SCENARIO_COMMENT_UPDATE => ['comment'],
        ];
    }
    
    public function rules()
    {
        return [
            [['name', 'comment'], 'required'],
            [['name'], 'string', 'min' => 2],
            [['comment'], 'string', 'max' => 1000],
            [['email'], 'email'],
        ];
    }
    
    public function save()
    {
        $sql = "INSERT INTO comment (id, name, email, comment) VALUES (null, '{$this->name}', '{$this->email}', '{$this->comment}')";
        return Yii::$app->db->createCommand($sql)->execute();
    }
    
    public static function getCommentList()
    { 
        $sql = 'SELECT * FROM comment'; 
        $result = Yii::$app->db->createCommand($sql)->queryAll();
       
        if(!empty($result) && is_array($result)){
            return $result;
        }
        else{
           return FALSE;
        }
        
    }
}
