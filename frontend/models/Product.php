<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property string $articul
 * @property string $date_published
 * @property integer $category_id
 * @property integer $price
 * @property integer $rating
 */
class Product extends ActiveRecord
{
    public static function tableName()
    {
        return '{{product}}';
    }
    
    public function rules()
    {
        return [
            [['name', 'articul', 'brend_id', 'price'], 'required'],
        ];
    }
    
    /**
     * @return string
     */
    public function getDatePublished()
    {
        return ($this->date_published) ? Yii::$app->formatter->asDate($this->date_published) : "Not set";
    }
    
    /**
     * @return string
     */
    public function getPrice()
    {
        return ($this->price) ? ($this->price) : "Not set";
    }
    
    /**
     * @return Brend|null
     */
    public function getBrend()
    {
        return $this->hasOne(Brend::className(), ['id' => 'brend_id'])->one();
    }
    
    /**
     * @return string
     */
    public function getBrendName()
    {
        if ($brend = $this->getBrend()) {
            return $brend->name;
        }
        return "Not set";
    }
    
    /**
     * @return ActiveQuery
     */
    public function getProductToCategoryRelations()
    {
        return $this->hasMany(ProductToCategory::className(), ['product_id' => 'id']);
    }
    
    /**
     * @return Author[]
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->via('productToCategoryRelations')->all();

    }
}

