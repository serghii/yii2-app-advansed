<?php

namespace frontend\models;

use \yii\db\ActiveRecord;

/**
 * This is the model class for table "brend".
 *
 * @property integer $id
 * @property string $name
 * @property string $country
 */
class Brend extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{brend}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'country'], 'string', 'max' => 255],
            [['name', 'country'], 'required'],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'country' => 'Country',
        ];
    }
}
