<?php

/*
 * This model request guantity of the news in the DB.
 */

namespace frontend\models;

use Yii;
/**
 * Description of Count
 *return integer 
 * @author Sergij Sergij
 */
class Count {
    
    public static function getQuantityRecords($tablename)
    {
        $tablename = strval($tablename);
        $sql = "SELECT COUNT(*) FROM $tablename";
        $result = Yii::$app->db->createCommand($sql)->queryOne();
        return $result['COUNT(*)'];
      
    }
}
