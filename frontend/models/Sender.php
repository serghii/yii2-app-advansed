<?php

namespace frontend\models;
use Yii;

/**
 * @author Sergij Sergij
 */
class Sender 
{
    public static function run($formData)
    {       
        $viewData = ['formData' => $formData];
        
        $result = Yii::$app->mailer->compose('/mailer/mailorder' , $viewData)
                ->setFrom('tex0987@gmail.com')
                ->setTo($formData['email'])
                ->setSubject('Window order.')
                ->send();
         if($result)
             {
             return true;
             }  
    }
}
