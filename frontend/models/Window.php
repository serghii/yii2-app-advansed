<?php

namespace frontend\models;

use yii\base\Model;
use frontend\models\Window;
use Yii;

/**
 * @author Sergij Sergij
 */
class Window extends Model
{
    const SCENARIO_ORDER_WINDOW = 'order_window';
    //const SCENARIO_ORDER_UPDATE = 'order_update';
    public $width;
    public $high;
    public $camera;
    public $sash;
    public $turnsash;
    public $color;
    public $sill;
    public $customerName;
    public $email;
    
    public function scenarios()
    {
        
        return [
            self::SCENARIO_ORDER_WINDOW => ['width', 'high', 'camera', 'sash', 'turnsash', 'color', 'sill', 'customerName', 'email'],
           // self::SCENARIO_ORDER_UPDATE => ['width', 'high', 'camera', 'sash', 'turnsash', 'color', 'sill', 'customerName', 'email'],
        ];
    }
    
     public function rules()
    {
        return [
            [['width', 'high', 'camera', 'sash', 'turnsash', 'color', 'sill', 'customerName', 'email'], 'required'],
            [['width'], 'string', 'min' => 2],
            [['high'], 'string', 'min' => 2],
            [['camera'], 'string', 'min' => 1],
            [['sash'], 'string', 'min' => 1],
            [['turnsash'], 'string', 'min' => 0],
            [['email'], 'email'],
            [['customerName'], 'string', 'min' => 2],
            //[['middleName'], 'required', 'on' => self::SCENARIO_EMPLOYEE_UPDATE],
        ];
    }
}
