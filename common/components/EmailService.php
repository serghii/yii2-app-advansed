<?php

namespace common\components;

use Yii;
use yii\base\Component;
use common\components\UserNotificationInterface;

/**
 * @author Sergij Sergij
 */
class EmailService extends Component{
        /**
     * @param UserNotificationInterface $user
     * @param string $subject
     * @return bool
     */
    public function notifyUser(UserNotificationInterface $event)
    {
        return Yii::$app->mailer->compose()
            ->setFrom('service.email@yii2frontend.com')
            //->setTo($user->getEmail())     
            ->setTo($event->getEmail())
            ->setSubject($event->getSubject())
            //->setSubject($subject)
            ->send();
    }

    /**
     * @param UserNotificationInterface $event
     * @return bool
     */
    public function notifyAdmins(UserNotificationInterface $event)
    {
        //echo '<pre>';
        //print_r($event);
        //echo '<pre>';die;
        
        return Yii::$app->mailer->compose()
            ->setFrom('service.email@yii2frontend.com')
            ->setTo('riodejaneiro2016@i.ua')
            ->setSubject($event->getSubject())
            //->setSubject($subject)
            ->send();
    }
}
