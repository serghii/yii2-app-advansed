<?php

namespace common\components;

use Yii;
/**
 * Description of StringHelper
 *
 * @author Sergij Sergij
 */
/**

class StringHelper {
    
    private  $limit;
    
public function __construct(){
    
    $this->limit = Yii::$app->params['shortTextLimit'];
}
    public function getShort($string, $limit = null){
        
        if ($limit === null){
            $limit = $this->limit;
        }
        
       return substr($string, 0, $limit); 
    }

}
 * 
 */
  class StringHelper {
 
    
    private  $limit;
    private  $number;
    
public function __construct(){
    
    $this->limit = Yii::$app->params['wordTextLimit'];
    $this->number = Yii::$app->params['shortTextLimit'];
}
    public function getShort($string, $limit = null){
        
        if ($limit === null){
            $limit = $this->limit;
        }
        if ($string === null){
            $string = $this->string;
        }
        
       $text = \yii\helpers\BaseStringHelper::truncateWords($string, $limit,$suffix = '...', $asHtml = false);
       return $text; 
    }
   
     public function getWords($string, $number = null){
        
        if ($number === null){
            $number = $this->number;
        }
        if ($string === null){
            $string = $this->string;
        }
        
       $fromhere = strpos ( $string , ' ' , $number );
       $text = \yii\helpers\BaseStringHelper::truncate( $string, $fromhere, '...', null, false );
       return $text; 
    }
    
}

