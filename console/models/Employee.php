<?php

namespace console\models;
use Yii;

/**
 * @author Sergij Sergij
 */
class Employee 
{
    //return array all data from table employee
    public static function getList()
    {
        $sql = 'SELECT * FROM employee';
        return Yii::$app->db->createCommand($sql)->queryAll();
    }
}
