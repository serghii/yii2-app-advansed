<?php

namespace console\models;

use Yii;
/**
 * @author Sergij Sergij
 */
class Sender 
{
    public static function run($subscribers,$newsList)
    {       
        $viewData = ['newsList' => $newsList];
        $count = 0;
        
        foreach ($subscribers as $subscriber)
        {
            $result = Yii::$app->mailer->compose('/mailer/newslist' , $viewData)
                    ->setFrom('tex0987@gmail.com')
                    ->setTo($subscriber['email'])
                    ->setSubject('Тема сообщения')
                    ->send();
            if($result){
                $count++;
            }
        }
        return $count;
    }
    
    public static function execute($employeeList)
    {
        //$viewData = ['employeeList' => $employeeList];
        $count = 0;
        $file = Yii::$app->params['logSendMails'];
        
        foreach ($employeeList as $employee)
        {
            $viewData = ['employee' => $employee];
            $result = Yii::$app->mailer->compose('/mailer/newsalary' , $viewData)
                    ->setFrom('tex0987@gmail.com')
                    ->setTo($employee['email'])
                    ->setSubject('Salary')
                    ->send();
            if($result){
                $count++;
                Yii::$app->formatter->timeZone = 'Europe/Kiev';
                $time = Yii::$app->formatter->asDate('now', 'php:d-m-Y H:i:s');
                //$file = '/var/www/project/console/runtime/logs/salarylog.txt';
                //echo $time;
                 $fp = fopen($file, 'a+');
                fwrite($fp, $time."\r\n");
                fclose($fp);
            }
        }
        return $count;
            
        
        
    }
}
