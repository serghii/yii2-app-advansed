<?php

namespace console\controllers;
use Yii;
use yii\helpers\Console;
use console\models\News;
use console\models\Subscriber;
use console\models\Sender;
use console\models\Employee;

/**
 * @author Sergij Bystrika
 * @actionWrite just sends mails 
 * @actionWrite sends mails and write 
 * the time sending in the log
 */
class MailerController  extends \yii\console\Controller
{
    /**
     * Sending newsletter
     */
    public function  actionSend()
    {    
        $newsList = News::getList();
        $subscribers = Subscriber::getList();
        $count = Sender::run($subscribers,$newsList); 
        Console::output("\nEmails sent: {$count}");
    }
    
    public function actionWrite()
    {
        $file = Yii::$app->params['logSendMails'];
        
        $result = Yii::$app->mailer->compose()
                ->setFrom('tex0987@gmail.com')
                ->setTo('tex0987@gmail.com')
                ->setSubject('Тема сообщения')
                ->setTextBody('Текст сообщения')
                ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
                ->send();
        var_dump($result);
        Yii::$app->formatter->timeZone = 'Europe/Kiev';
        $time = Yii::$app->formatter->asDate('now', 'php:d-m-Y H:i:s');
        //$file = '/var/www/project/frontend/web/log.txt';
        echo $time;
        $fp = fopen($file, 'a+');
        fwrite($fp, $time."\r\n");
        fclose($fp);
        die;
        
    }
    
    public function actionSalary()
    {   $file = Yii::$app->params['logSendMails'];
        $employeeList = Employee::getList();
        //print_r($employeeList);
        $count = Sender::execute($employeeList); 
        Console::output("\nEmails sent for {$count} employee and write in {$file}");
        //echo 'Hello from salary';die;
    }
    
}
