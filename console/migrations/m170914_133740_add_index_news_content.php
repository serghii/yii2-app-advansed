<?php

use yii\db\Migration;

class m170914_133740_add_index_news_content extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE news ADD FULLTEXT INDEX idx_content (content)");
    }

    public function down()
    {
        return $this->dropIndex('idx_content', 'news');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_133740_add_index_news_content cannot be reverted.\n";

        return false;
    }
    */
}
