<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comment`.
 */
class m170815_163238_create_comment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'email' => $this->string()->unique(),
            'comment' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('comment');
    }
}
