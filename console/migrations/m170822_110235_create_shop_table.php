<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop`.
 */
class m170822_110235_create_shop_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createProducts();
        $this->createBrends();
        $this->createCategories();
        $this->createProductsToCategories();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product');
        $this->dropTable('brend');
        $this->dropTable('category');
        $this->dropTable('product_to_category');
    }
    
     private function createProducts()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'articul' => $this->string(),
            'date_published' => $this->date(),
            'brend_id' => $this->integer(),
            'price' => $this->integer(),
            'rating' => $this->integer(),
        ]);
        $this->insert('product', [
            'id' => 1,
            'name' => 'Cube-450X',
            'articul' => '8934638534',
            'date_published' => '2015-01-21',
            'brend_id' => 1,
            'price' => '300',
            'rating' => '75',
        ]);
    }
    
    private function createBrends()
    {
        $this->createTable('brend', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'country' => $this->string(),
        ]);

        $this->insert('brend', [
            'id' => 1,
            'name' => 'Cube',
            'country' => 'China',
        ]);
    }
    
    private function createCategories()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'identity_number' => $this->integer(),
        ]);

        $this->insert('category', [
            'id' => 1,
            'name' => 'Tablets',
            'identity_number' => '1',
        ]);
    }
    
    private function createProductsToCategories()
    {
        $this->createTable('product_to_category', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'category_id' => $this->integer(),
        ]);
        
        $this->insert('product_to_category', [
            'id' => 1,
            'product_id' => 1,
            'category_id' => 1,
        ]);
    }
}
