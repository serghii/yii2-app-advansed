<?php
return [
    'adminEmail' => 'admin@example.com',
    'logSendMails' => '/var/www/project/console/runtime/logs/salarylog.txt',
    'shortTextLimit' => 40,
    'wordTextLimit' => 8,
];
